package com.ydl.iec.iec104.config;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.ContentTypeDelegatingMessageConverter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;

import javax.annotation.Resource;

/**
 * RabbitMQ配置类
 */
@Configuration
public class RabbitMQConfig implements InitializingBean {
    /**
     * 自动注入RabbitTemplate模板
     */
    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息JSON序列化
     */
    @Override
    public void afterPropertiesSet() {
        //使用JSON序列化
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
    }
    //队列名称
    public static final String ITEM_QUEUE = "itemQueue";
    //队列名称
    public static final String TRANSFORMERS_QUEUE = "transformersQueue";
    //队列名称
    public static final String LINECURRENTS_QUEUE = "lineCurrentsQueue";
    //队列名称
    public static final String HISTORY_QUEUE = "historyMessage";
    //队列名称
    public static final String VALTAGE_BARS_QUEUE = "valtageBarData";
    //实时出力监听队列
    public static final String REAL_TIME_MESSAGE_QUEUE = "realTimeMessageQueue";

    @Bean("realTimeMessageQueue")
    public Queue realTimeMessageQueue(){
        return new Queue(REAL_TIME_MESSAGE_QUEUE,true);
    }

    @Bean("itemQueue")
    public Queue itemQueue(){
        return new Queue(ITEM_QUEUE,true);
    }

    @Bean("transformersQueue")
    public Queue transFormerQueue(){
        return new Queue(TRANSFORMERS_QUEUE,true);
    }

    @Bean("lineCurrentsQueue")
    public Queue lineCurrentsQueue(){
        return new Queue(LINECURRENTS_QUEUE,true);
    }

    @Bean("historyMessage")
    public Queue historyMessage(){
        return new Queue(HISTORY_QUEUE,true);
    }

    @Bean("valtageBarData")
    public Queue valtageBarData(){
        return new Queue(VALTAGE_BARS_QUEUE,true);
    }


    @Bean
    public MessageConverter messageConverter() {
        ContentTypeDelegatingMessageConverter messageConverter = new ContentTypeDelegatingMessageConverter();
        messageConverter.addDelegate(MediaType.APPLICATION_JSON_VALUE, new Jackson2JsonMessageConverter());
        messageConverter.addDelegate(MediaType.TEXT_PLAIN_VALUE, new SimpleMessageConverter());
        return messageConverter;
    }

}
