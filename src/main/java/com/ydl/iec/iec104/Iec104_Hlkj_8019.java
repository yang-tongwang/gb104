package com.ydl.iec.iec104;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
@ComponentScan({"com.ydl.iec"})
public class Iec104_Hlkj_8019 {

    public static void main(String[] args) {
        SpringApplication.run(Iec104_Hlkj_8019.class, args);
    }

}
