package com.ydl.iec.iec104.message;

public abstract class InformationElement {
    public abstract int encode(byte[] buffer, int i);
}
