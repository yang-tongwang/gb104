package com.ydl.iec.iec104.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description:
 * @author: hwx
 * @date: 2022/09/26
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TdenginData implements Serializable {
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	public String getValue3() {
		return value3;
	}

	public void setValue3(String value3) {
		this.value3 = value3;
	}

	public String getValue4() {
		return value4;
	}

	public void setValue4(String value4) {
		this.value4 = value4;
	}

	public String getValue5() {
		return value5;
	}

	public void setValue5(String value5) {
		this.value5 = value5;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	private static final long serialVersionUID = 1L;
	/**
	 * 设备描述
	 *
	 */
	private String name;
	/**
	 * 值
	 *
	 */
	private String value1;
	/**
	 * 值
	 *
	 */
	private String value2;
	/**
	 * 值
	 *
	 */
	private String value3;
	/**
	 * 值
	 *
	 */
	private String value4;
	/**
	 * 值
	 *
	 */
	private String value5;
	/**
	 * 值
	 *
	 */
	private String value6;
	/**
	 * 值
	 *
	 */
	private String value7;
	/**
	 * 值
	 *
	 */
	private String value8;

	/**
	 * 时间
	 *
	 */
	private String time;

	public TdenginData(String name) {
		this.name = name;
	}

	public TdenginData(String name, String value1) {
		this.name = name;
		this.value1 = value1;
	}

	public TdenginData(String name, String value1, String value2) {
		this.name = name;
		this.value1 = value1;
		this.value2 = value2;
	}

	public TdenginData(String name, String value1, String value2, String value3) {
		this.name = name;
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
	}

	public TdenginData(String name, String value1, String value2, String value3, String value4) {
		this.name = name;
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
	}


	public TdenginData(String name, String value1, String value2, String value3, String value4, String value5) {
		this.name = name;
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
		this.value5 = value5;
	}

	public TdenginData(String name, String value1, String value2, String value3, String value4, String value5, String value6) {
		this.name = name;
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
		this.value5 = value5;
		this.value6 = value6;
	}

	public TdenginData(String name, String value1, String value2, String value3, String value4, String value5, String value6, String value7) {
		this.name = name;
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
		this.value5 = value5;
		this.value6 = value6;
		this.value7 = value7;
	}

	public TdenginData(String name, String value1, String value2, String value3, String value4, String value5, String value6, String value7, String value8) {
		this.name = name;
		this.value1 = value1;
		this.value2 = value2;
		this.value3 = value3;
		this.value4 = value4;
		this.value5 = value5;
		this.value6 = value6;
		this.value7 = value7;
		this.value8 = value8;
	}
}
