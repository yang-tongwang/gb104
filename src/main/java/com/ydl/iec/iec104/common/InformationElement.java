package com.ydl.iec.iec104.common;

public abstract class InformationElement {
    public abstract int encode(byte[] buffer, int i);
}
