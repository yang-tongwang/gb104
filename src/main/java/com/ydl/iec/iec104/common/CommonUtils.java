package com.ydl.iec.iec104.common;

/**
 * @description:
 * @author: hwx
 * @date: 2022/09/26
 **/
public class CommonUtils {
	public static String getSbms(int i) {
		if (i <= 5) {
			return "温湿度1（TTAH1）";
		} else if (i <= 10) {
			return "温湿度2（TTAH2）";
		} else if (i <= 13) {
			return "水浸1（TWTR1）";
		} else if (i <= 16) {
			return "水浸2（TWTR2）";
		} else if (i <= 18) {
			return "六氟化硫1（TSF61）";
		}else if (i <= 20) {
			return "六氟化硫2（TSF62）";
		}else if (i <= 23) {
			return "烟感1（TSMK1）";
		}else if (i <= 26) {
			return "烟感2（TSMK2）";
		}else if ( i == 27) {
			return "风机（TFAN）";
		}else if( i== 28){
			return "灯光（TLIG）";
		}else if (i <= 36) {
			return "空调（TACD）";
		}else {
			return "局放（TPDS）";
		}
	}

	public static String getdms(int i) {
		switch (i) {
			case 1:
				return "温度";
			case 2:
				return "湿度";
			case 3:
				return "设备状态";
			case 4:
				return "电池电压";
			case 5:
				return "电池剩余电量";
			case 6:
				return "温度";
			case 7:
				return "湿度";
			case 8:
				return "设备状态";
			case 9:
				return "电池电压";
			case 10:
				return "电池剩余电量";
			case 11:
				return "电池电压";
			case 12:
				return "电池剩余电量";
			case 13:
				return "水浸状态";
			case 14:
				return "电池电压";
			case 15:
				return "电池剩余电量";
			case 16:
				return "水浸状态";
			case 17:
				return "氧气浓度";
			case 18:
				return "六氟化硫浓度";
			case 19:
				return "氧气浓度";
			case 20:
				return "六氟化硫浓度";
			case 21:
				return "电池剩余电量";
			case 22:
				return "电池电压";
			case 23:
				return "消防状态";
			case 24:
				return "电池剩余电量";
			case 25:
				return "电池电压";
			case 26:
				return "消防状态";
			case 27:
				return "风机状态";
			case 28:
				return "灯光状态";
			case 29:
				return "空调状态";
			case 30:
				return "A相电流";
			case 31:
				return "B相电流";
			case 32:
				return "C相电流";
			case 33:
				return "空调模式";
			case 34:
				return "空调风速";
			case 35:
				return "空调风摆";
			case 36:
				return "空调温度";
			case 37:
				return "局放采样峰值";
			case 38:
				return "峰值相位";
			case 39:
				return "局部脉冲次数";

			default:
				return "";
		}
	}

	public static String getdh(int i) {
		switch (i) {
			case 1:
				return "4001";
			case 2:
				return "4002";
			case 3:
				return "4003";
			case 4:
				return "4004";
			case 5:
				return "4005";
			case 6:
				return "4006";
			case 7:
				return "4007";
			case 8:
				return "4008";
			case 9:
				return "4009";
			case 10:
				return "4010";
			case 11:
				return "4011";
			case 12:
				return "4012";
			case 13:
				return "4013";
			case 14:
				return "4014";
			case 15:
				return "4015";
			case 16:
				return "4016";
			case 17:
				return "4017";
			case 18:
				return "4018";
			case 19:
				return "4019";
			case 20:
				return "4020";
			case 21:
				return "4021";
			case 22:
				return "4022";
			case 23:
				return "4023";
			case 24:
				return "4024";
			case 25:
				return "4025";
			case 26:
				return "4026";
			case 27:
				return "4027";
			case 28:
				return "4028";
			case 29:
				return "4029";
			case 30:
				return "4030";
			case 31:
				return "4031";
			case 32:
				return "4032";
			case 33:
				return "4033";
			case 34:
				return "4034";
			case 35:
				return "4035";
			case 36:
				return "4036";
			case 37:
				return "4037";
			case 38:
				return "4038";
			case 39:
				return "4039";
			default:
				return "0";
		}
	}
}
