package com.ydl.iec.iec104.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description:
 * @author: hwx
 * @date: 2022/09/26
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TelemeteringData implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 设备描述
	 *
	 */
	private String sbms;
	/**
	 * 点描述
	 *
	 */
	private String dms;
	/**
	 * 转发序号
	 *
	 */
	private int zfxh;
	/**
	 * 点好
	 *
	 */
	private String dh;
	/**
	 * 值
	 *
	 */
	private String value;
	/**
	 * 设备id
	 *
	 */
	private String entityId;
}
