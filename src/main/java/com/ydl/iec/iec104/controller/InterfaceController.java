package com.ydl.iec.iec104.controller;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @ClassName: Iec104ServerInitializer
 * @Description: 对外接口
 * @author YTW
 * @date 2022年9月23日
 */
@RestController
@RequestMapping("/interfaceApi")
public class InterfaceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterfaceController.class);

    @Autowired
    private AmqpTemplate rabbitTemplate;

    /**
     * 提供给佳源接口，获取图片数据
     * @param data
     * @return
     */
    @PostMapping("/jykjApiData")
    public Object jykjApiData(@RequestBody Object data){
        Map<String,Object> map = new HashMap<>();
        if (!StringUtils.isEmpty(data)) {
            //打印接受到的数据
            String jsonStr = JSON.toJSONString(data);
//            LOGGER.info("收到图片信息了:"+jsonStr);
            map = JSON.parseObject(jsonStr,Map.class);
        }
        if (!StringUtils.isEmpty (map) && map.containsKey ("alarmType")) {
            //推送AI告警信息
            rabbitTemplate.convertAndSend ("jyAiGj", map);
        }else if (!StringUtils.isEmpty (map) && map.containsKey ("snapeImage")) {
            //推送实时监控画面
            rabbitTemplate.convertAndSend ("jyRealMonitoring", map);
        }

        return map;
    }
}
